#ifndef __SCREEN_H
#define __SCREEN_H

#define COLOR 0x70

int x = 0;
int y = 0;

unsigned char * videoram = (unsigned char *) 0xb8000;

void move();
void clear();

void putchar(unsigned char);
void puts(unsigned char *);

#endif
