#ifndef __SYSTEM_H
#define __SYSTEM_H

unsigned char inportb (unsigned short);
void outportb (unsigned short, unsigned char);

#endif
