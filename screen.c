#include "screen.h"

void move() 
{
  if (x%80 == 79) 
  {
    if (y == 25) y = 0;
    else y = y + 1;
    x = 0;
  }
  int i = 80*y + x;
  outportb(0x3D4, 14);
  outportb(0x3D5, i >> 8);
  outportb(0x3D4, 15);
  outportb(0x3D5, i);
}

void clear() 
{
  int i = 0;
  while (i < (80*25*2)) 
  {
    videoram[i++] = ' ';
    videoram[i++] = COLOR;
  }
  x = 0;
  y = 0;
  move();
}

void putchar(unsigned char str) 
{
  int i = 80*y + x;
  unsigned char * videoram = (unsigned char *) 0xb8000;
  videoram[i++] = str;
  videoram[i] = COLOR;
  x = x + 2;
  move();
}

void puts(unsigned char * str) 
{
  int i = 80*y + x;
  while (*str != '\0') 
  {
    putchar(*str++);
  }
}

