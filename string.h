#ifndef __STRING_H
#define __STRING_H
#define NULL '\0'

unsigned char * memcpy(unsigned char *, const unsigned char *, int);
unsigned char * strcpy(unsigned char *, const unsigned char *);
unsigned char * strncpy(unsigned char *, const unsigned char *, int);

unsigned char * strcat(unsigned char *, const unsigned char *);
unsigned char * strncat(unsigned char *, const unsigned char *, int);

int memcmp(const void *, const void *, int);
int strcmp(const unsigned char *, const unsigned char *);

unsigned char * memset(unsigned char *, unsigned char, int);
unsigned short * memsetw(unsigned short *, unsigned short, int);

int strlen(const char *);

#endif
