#include "string.h"

unsigned char * memcpy(unsigned char * dest, const unsigned char * src, int count)
{
  int i = 0;
  while (i < count) 
  {
    dest[i] = src[i];
    i++;
  }
  return dest;
}

unsigned char * strcpy(unsigned char * dest, const unsigned char * src) 
{
  int i = 0;
  while (src[i] != NULL) 
  {
    dest[i] = src[i];
    i++;
  }
  return dest;
}

unsigned char * strncpy(unsigned char * dest, const unsigned char * src, int count) 
{
  int i = 0;
  while (i < count) 
  {
    dest[i] = src[i];
    i++;
  }
  return dest;
}

unsigned char * strcat(unsigned char * dest, const unsigned char * src) 
{
  int j = 0;
  while (dest[j] != NULL) 
  {
    j++;
  }
  
  int i = 0;
  while (src[i] != NULL) 
  {
    dest[j++] = src[i++];
  }
  return dest;
}

unsigned char * strncat(unsigned char * dest, const unsigned char * src, int count) 
{
  int j = 0;
  while (dest[j] != NULL) 
  {
    j++;
  }
  
  int i = 0;
  while (i < count) 
  {
    dest[j++] = src[i++];
  }
  return dest;
}

unsigned char * memset(unsigned char * dest, unsigned char val, int count)
{
  int i = 0;
  while (i < count) 
  {
    dest[i++] = val;
  }
  return dest;
}

unsigned short * memsetw(unsigned short * dest, unsigned short val, int count)
{
  int i = 0;
  while (i < count) 
  {
    dest[i++] = val;
  }
  return dest;
}

int strlen(const char * str)
{
  int i = 0;
  while (*str++ != NULL) i++;
  return i; 
}

