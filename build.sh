#!/bin/sh
nasm -f elf -o loader.o loader.s
gcc -fno-builtin-log -o system.o -c system.c
gcc -fno-builtin-log -o string.o -c string.c
gcc -fno-builtin-log -o screen.o -c screen.c
gcc -fno-builtin-log -o kernel.o -c kernel.c
ld -T linker.ld -o kernel.bin loader.o system.o string.o screen.o kernel.o
rm *.o
